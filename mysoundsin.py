import unittest
from mysound import Sound


class SoundSin(Sound):
    def __init__(self, duration, frequency, amplitude):
        super().__init__(duration)  # Inicializar clase padre

        # Llamar a la función sin, con Sound.sin, justo después de
        # inicializar la clase
        self.sin(frequency, amplitude)

        # Guardar los parámetros como atributos
        self.frequency = frequency
        self.amplitude = amplitude


"""
Idea principal:
- La clase SoundSin hereda de Sound, por lo que: En __init__ se llama primero a
  super().__init__(duration) para inicializar la clase padre
- Luego se llama a self.sin() para generar la señal sinusoidal con los
  parámetros dados
- Se almacenan frequency y amplitude como atributos
"""
