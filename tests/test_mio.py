import unittest
from mysound import Sound
import time


class TestSoundTiming(unittest.TestCase):

    def test_timing(self):
        # Generar primera señal
        t1 = time.time() # Tiempo inicial
        sound1 = Sound(5)
        sound1.sin(440, 10000)
        t2 = time.time() # Tiempo final

        #Generar segunda señal
        t3 = time.time()
        sound2 = Sound(5)
        sound2.sin(880, 10000)
        t4 = time.time()

        #Comparar tiempos de generación
        gen_time1 = t2 - t1
        gen_time2 = t4 - t3

        self.assertLess(gen_time1, 0.1) # Comprobar tiempo razonable
        self.assertLess(gen_time2, 0.1) # para señal de 1 segundo

        # Comparar tiempos entre señales
        self.assertAlmostEqual(gen_time1, gen_time2, delta=0.02)


if __name__ == '__main__':
    unittest.main()

""""
Idea principal:
- Tomar tiempo antes y después de generar cada señal
- Calcular el tiempo de generación restando marcas de tiempo
- Comparar que los tiempos sean razonables para señales de 1 seg
- Comparar que los tiempos de generación de ambas señales sean casi iguales
"""
